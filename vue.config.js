module.exports = {
    publicPath: './static',
    pluginOptions: {
        quasar: {
            importStrategy: 'manual',
            rtlSupport: true,
        },
    },
    transpileDependencies: [
        'quasar',
    ],
    pages: {
        top: {
            entry: 'src/frontend/main.js',
            template: 'public/index.html',
            filename: 'index.html'
        }
    },
    lintOnSave: false
};