package main

import (
	router "puzzle-tool-v2/src/backend/router"
)

func main() {
	r := router.CreateRoute()
	r.Run(":8080")
}
