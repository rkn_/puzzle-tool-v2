package libs

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

// Connect connect Postgres SQL DB server
func Connect() *sql.DB {
	password := "dummy"
	db, err := sql.Open("postgres", "host=127.0.0.1 port=5432 user=lkeix password=" + password + " dbname=puzzleTool sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	return db
}
