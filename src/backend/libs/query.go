package libs

import (
	"fmt"
	"strconv"
	"time"
)

// Patient is a datastructure
type Patient struct {
	PatientID int64
	Name      string
	Age       int64
	Gender    string
}

// PatientData is selected patient data
type PatientData struct {
	ContainerID int64
	Date        string
	Explain     string
	Work        string
	Check       string
}

// ReadInstituteID is called when doctor resisteration
func ReadInstituteID(instituteName string) int64 {
	query := "select InstituteID from Institute where name = $1"
	args := []interface{}{
		instituteName,
	}
	data, err := GetRow(query, args)
	if err {
		return -1
	}
	return data[0][0].(int64)
}

// InsertDoctor is called when doctor resisteration
func InsertDoctor(doctorid, cryptedpassword string, ismaster bool, instituteid int64) bool {
	query := "insert into doctor (doctorid, password, master, instituteid) values ($1, $2, $3, $4)"
	args := []interface{}{
		doctorid,
		cryptedpassword,
		ismaster,
		instituteid,
	}
	return Exec(query, args)
}

// InsertPatient is called when patient resisteration
func InsertPatient(name, age string, gender bool) string {
	query := "insert into patient (name, age, gender) values ($1, $2, $3) RETURNING patientid"
	args := []interface{}{
		name,
		age,
		gender,
	}
	data, _ := GetRow(query, args)
	if len(data) > 0 {
		res := data[0][0].(int64)
		return strconv.Itoa(int(res))
	}
	return ""
}

// InsertContentStart is called when process is ended
func InsertContentStart(now, doctorID, patientID string) string {
	query := "insert into container (date, doctorid, patientid) values ($1, $2, $3) RETURNING id"
	args := []interface{}{
		now,
		doctorID,
		patientID,
	}
	data, _ := GetRow(query, args)
	if len(data) > 0 {
		res := data[0][0].(int64)
		return strconv.Itoa(int(res))
	}
	return ""
}

// InsertContent is called when process is ended
func InsertContent(continerID, jsonStr string, kind int) {
	query := ""
	if kind == 0 {
		query = "update container set explain = $1 where id = $2"
	} else if kind == 1 {
		query = "update container set work = $1 where id = $2"
	} else if kind == 2 {
		query = "update container set puzzlecheck = $1 where id = $2"
	}
	args := []interface{}{
		jsonStr,
		continerID,
	}
	Exec(query, args)
}

// ReadContents is called when doctor display patient data
func ReadContents(doctorID string, isMaster bool) []Patient {
	query := "select distinct patient.patientid, patient.name, patient.age, patient.gender " +
		"from patient, container where container.doctorid = $1 and container.patientid = patient.patientid"
	if isMaster {
		// サブクエリを含むクエリを発行
		query = "select distinct patient.patientid, patient.name, patient.age, patient.gender " +
			"from patient, container, institute where container.doctorid in (select doctor.doctorid from doctor where doctor.instituteid in ( " +
			"select doctor.instituteid from doctor where doctor.doctorid = $1 ) )"
	}
	args := []interface{}{
		doctorID,
	}
	data, _ := GetRow(query, args)
	var res []Patient
	for _, val := range data {
		var patient Patient
		patient.PatientID = val[0].(int64)
		patient.Name = val[1].(string)
		patient.Age = val[2].(int64)
		patient.Gender = "男性"
		if !val[3].(bool) {
			patient.Gender = "女性"
		}
		res = append(res, patient)
	}
	return res
}

// ReadDoctorIsMaster is called when conform doctor is master user or not.
func ReadDoctorIsMaster(doctorID string) bool {
	query := "select master from doctor where doctorid = $1"
	args := []interface{}{
		doctorID,
	}
	data, _ := GetRow(query, args)
	if len(data) > 0 {
		return data[0][0].(bool)
	}
	return false
}

// ReadPatientContents is called when patient data
func ReadPatientContents(doctorID, patientID string) {
}

// ReadDoctorPassword is called when doctor's authorization
func ReadDoctorPassword(doctorID string) string {
	query := "select password from doctor where doctorid = $1"
	args := []interface{}{
		doctorID,
	}
	data, _ := GetRow(query, args)
	if len(data) > 0 {
		return data[0][0].(string)
	}
	return ""
}

// ReadPatients is called when call readPatients api
func ReadPatients(doctorID string) []string {
	query := "select distinct patient.name from patient, container where container.doctorid = $1 and patient.patientid = container.patientid"
	args := []interface{}{
		doctorID,
	}
	data, _ := GetRow(query, args)
	var res []string
	if len(data) > 0 {
		for _, val := range data {
			res = append(res, val[0].(string))
		}
	}
	return res
}

// ReadPatientID is called when already start process
func ReadPatientID(name, doctorID string) string {
	query := "select distinct patient.patientid from patient, container where container.doctorid = $1 and patient.name = $2 and container.patientid = patient.patientid"
	args := []interface{}{
		doctorID,
		name,
	}
	data, _ := GetRow(query, args)
	if len(data) > 0 {
		res := data[0][0].(int64)
		return strconv.Itoa(int(res))
	}
	return ""
}

// ReadPatientData is called when selected patient data read
func ReadPatientData(patientID, doctorID string) []PatientData {
	ismaster := ReadDoctorIsMaster(doctorID)
	query := "select id, date, explain, work, puzzlecheck from container where doctorid = $1 and patientid = $2"
	args := []interface{}{
		doctorID,
		patientID,
	}
	if ismaster {
		query = "select id, date, explain, work, puzzlecheck from container where patientid = $1"
		args = []interface{}{
			patientID,
		}
	}
	data, _ := GetRow(query, args)
	var res []PatientData
	if len(data) > 0 {
		for _, val := range data {
			var tmp PatientData
			tmp.ContainerID = val[0].(int64)
			t := val[1].(time.Time)
			layout := "2006/01/02"
			tmp.Date = t.Format(layout)
			tmp.Explain = string(val[2].([]uint8))
			tmp.Work = string(val[3].([]uint8))
			tmp.Check = string(val[4].([]uint8))
			res = append(res, tmp)
		}
	}
	return res
}

// DeleteContainerData is called delete patient data api
func DeleteContainerData(containerID int) {
	query := "delete from container where id = $1"
	args := []interface{}{
		containerID,
	}
	fmt.Println(Exec(query, args))
}
