package api

import (
	"encoding/json"
	"fmt"
	"puzzle-tool-v2/src/backend/libs"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// ReadPassedPatient is called when readpassedpatient API
func ReadPassedPatient(ctx *gin.Context) {
	session := sessions.Default(ctx)
	doctorid := session.Get("userid").(string)
	master := libs.ReadDoctorIsMaster(doctorid)
	patients := libs.ReadContents(doctorid, master)
	fmt.Println(patients)
	jsonStr, _ := json.Marshal(patients)
	ctx.Writer.Write(jsonStr)
}

// ReadPatientData is called when read patient data api
func ReadPatientData(ctx *gin.Context) {
	session := sessions.Default(ctx)
	req := ctx.Request
	req.ParseForm()
	patientID := req.PostFormValue("patientID")
	doctorID := session.Get("userid").(string)
	res := libs.ReadPatientData(patientID, doctorID)
	jsonStr, _ := json.Marshal(res)
	ctx.Writer.Write(jsonStr)
}

// DeletePatientData is called when doctor remove patient old data
func DeletePatientData(ctx *gin.Context) {
	req := ctx.Request
	req.ParseForm()
	containerID := req.PostFormValue("containerID")
	var containerIDs []int
	json.Unmarshal([]byte(containerID), &containerIDs)
	fmt.Println(containerIDs)
	for _, val := range containerIDs {
		libs.DeleteContainerData(val)
	}
}
