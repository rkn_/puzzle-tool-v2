package api

import (
	"encoding/json"
	"fmt"
	"puzzle-tool-v2/src/backend/libs"
	"time"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// ReadPatients read patients data
func ReadPatients(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	patients := libs.ReadPatients(userid)
	jsonStr, _ := json.Marshal(patients)
	ctx.Writer.Write(jsonStr)
}

// StartProcess start timestump process
func StartProcess(ctx *gin.Context) {
	req := ctx.Request
	req.ParseForm()
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	patientid := ""
	patientType := req.PostFormValue("type")
	name := req.PostFormValue("name")
	fmt.Println(patientType)
	if patientType == "new" {
		gender := req.PostFormValue("gender")
		age := req.PostFormValue("age")
		res := true
		if gender == "女性" {
			res = false
		}
		patientid = libs.InsertPatient(name, age, res)
	} else {
		patientid = libs.ReadPatientID(name, userid)
	}
	now := getNow()
	containerid := libs.InsertContentStart(now, userid, patientid)
	jsonStr, _ := json.Marshal(containerid)
	ctx.Writer.Write(jsonStr)
}

// InsertTimeStump is called when insertTimeStump API
func InsertTimeStump(ctx *gin.Context) {
	req := ctx.Request
	req.ParseForm()
	timeStumpType := req.PostFormValue("type")
	eval := req.PostFormValue("timestump")
	containerID := req.PostFormValue("containerid")
	fmt.Println(eval)
	if timeStumpType == "exp" {
		libs.InsertContent(containerID, eval, 0)
	} else if timeStumpType == "work" {
		libs.InsertContent(containerID, eval, 1)
	} else if timeStumpType == "check" {
		libs.InsertContent(containerID, eval, 2)
	}
}

func getNow() string {
	t := time.Now()
	layout := "2006-01-02"
	return t.Format(layout)
}
