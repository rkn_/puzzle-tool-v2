package api

import (
	"encoding/json"
	"fmt"
	"puzzle-tool-v2/src/backend/libs"
	"puzzle-tool-v2/src/backend/utils"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// LoginRes login process result structure
type LoginRes struct {
	Sess string
}

// Login is called when authorization process
func Login(ctx *gin.Context) {
	session := sessions.Default(ctx)
	req := ctx.Request
	id := req.PostFormValue("ID")
	sess := ""
	var res LoginRes
	password := req.PostFormValue("Password")
	if authorization(id, password) {
		sess = utils.GenerateID()
		session.Set("userid", id)
		session.Set("sessid", sess)
		session.Save()
		res.Sess = sess
	}
	jsonStr, _ := json.Marshal(res)
	ctx.Writer.Write(jsonStr)
}

// Signup is called when signup process
func Signup(ctx *gin.Context) {
	session := sessions.Default(ctx)
	req := ctx.Request
	institute := req.PostFormValue("Institute")
	id := req.PostFormValue("ID")
	password := req.PostFormValue("Password")
	masterKey := req.PostFormValue("MasterKey")
	defaultMaster := "xbkYF6UbDu7LW8Xf"
	// マスターユーザか確認
	master := false
	hashed := utils.CryptPasswd(password)
	if masterKey == defaultMaster {
		master = true
	}
	instituteID := libs.ReadInstituteID(institute)
	ok := libs.InsertDoctor(id, hashed, master, instituteID)
	var res LoginRes
	if ok {
		sess := utils.GenerateID()
		session.Set("userid", id)
		session.Set("sessid", sess)
		session.Save()

		res.Sess = sess
	}
	fmt.Println(res.Sess)
	jsonStr, _ := json.Marshal(res)
	ctx.Writer.Write(jsonStr)
}

// ConfirmSession confirm session process
func ConfirmSession(ctx *gin.Context) {
	session := sessions.Default(ctx)
	req := ctx.Request
	req.ParseForm()
	onsess := session.Get("sessid")
	if onsess != nil {
		sess := req.PostFormValue("sess")
		fmt.Println(onsess)
		fmt.Println(sess)
		if onsess == sess {
			ctx.Writer.WriteString("true")
		} else {
			ctx.Writer.WriteString("false")
		}
	} else {
		ctx.Writer.WriteString("false")
	}
}

// Logout is called when logout process
func Logout(ctx *gin.Context) {
	session := sessions.Default(ctx)
	session.Clear()
	session.Save()
}

func authorization(id, password string) bool {
	hashed := libs.ReadDoctorPassword(id)
	fmt.Println(utils.ComparePasswd(password, hashed))
	return utils.ComparePasswd(password, hashed)
}
