package api

import (
	"encoding/csv"
	"encoding/json"
	"puzzle-tool-v2/src/backend/utils"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

// FileData is filedata
type FileData struct {
	Data   [][]int
	Result []float64
	Ratios []float64
}

// GetFile is an end point to get file
func GetFile(ctx *gin.Context) {
	req := ctx.Request
	f, _, err := req.FormFile("file")
	reader := csv.NewReader(f)
	x, y := utils.ReadData(reader)
	sectionString := req.FormValue("sectionbase")
	if err != nil {
		return
	}
	sections := strings.Split(sectionString, ",")
	rx, ry := utils.SplineInterporation(x, y)
	var Res FileData
	Res.Data = convXY(x, y)
	Res.Result, Res.Ratios = utils.Integrate(rx, ry, sections)
	jsonres, _ := json.Marshal(Res)
	ctx.Writer.Write(jsonres)
}

func convXY(x, y []string) [][]int {
	var res [][]int
	for i := 0; i < len(x); i++ {
		var tmp []int
		xVal, _ := strconv.Atoi(x[i])
		yVal, _ := strconv.Atoi(y[i])
		tmp = append(tmp, xVal)
		tmp = append(tmp, yVal)
		res = append(res, tmp)
	}
	return res
}
