package utils

import (
	"fmt"
	"math"
	"strconv"
)

func strconvfloat64(x, y []string) ([]float64, []float64) {
	var xf []float64
	var yf []float64
	for key := 0; key < len(x); key++ {
		xval, _ := strconv.ParseFloat(x[key], 64)
		yval, _ := strconv.ParseFloat(y[key], 64)
		xf = append(xf, xval)
		yf = append(yf, yval)
	}
	return xf, yf
}

func calc(a, b, c, d []float64, t float64) float64 {
	i := int(math.Floor(t))
	if i < 0 {
		i = 0
	} else if i >= len(a) {
		i = len(a) - 1
	}
	dt := t - float64(i)
	result := a[i] + (b[i]+(c[i]+d[i]*dt)*dt)*dt
	return result
}

func initialize(x, y []float64, ndata int) ([]float64, []float64, []float64, []float64, []float64) {
	var a, b, c, d, w []float64
	cnt := 1
	for i := 0; i <= ndata; i++ {
		// 次のデータも見て進めるか判断する
		if cnt != len(x)-1 {
			if int(x[cnt+1]) == i {
				i--
				cnt++
			}
		}
		if int(x[cnt]) == i {
			a = append(a, y[cnt])
			cnt++
		} else {
			a = append(a, y[cnt-1])
		}
	}
	fmt.Println(a)
	for i := 0; i <= ndata; i++ {
		if i == 0 || i == ndata {
			c = append(c, 0.0)
		} else {
			c = append(c, 3.0*(a[i-1]-2.0*a[i]+a[i-1]))
		}
	}
	for i := 0; i < ndata; i++ {
		if i == 0 {
			w = append(w, 0.0)
		} else {
			tmp := 4.0 - w[i-1]
			c[i] = (c[i] - c[i-1]) / tmp
			w = append(w, 1.0/tmp)
		}
	}
	for i := ndata - 1; i > 0; i-- {
		c[i] = c[i] - c[i+1]*w[i]
	}
	for i := 0; i <= ndata; i++ {
		if i == ndata {
			d = append(d, 0.0)
			b = append(b, 0.0)
		} else {
			d = append(d, (c[i+1]-c[i])/2.0)
			b = append(b, a[i+1]-a[i]-c[i]-d[i])
		}
	}
	return a, b, c, d, w
}

// SplineInterporation called by getfile api
func SplineInterporation(x, y []string) ([]float64, []float64) {
	xf, yf := strconvfloat64(x, y)
	a, b, c, d, _ := initialize(xf, yf, int(xf[len(xf)-1])-1)
	var rx, ry []float64
	n, _ := strconv.Atoi(x[len(x)-1])
	for i := 0.0; i < float64(n); i += 0.01 {
		rx = append(rx, i)
		ry = append(ry, calc(a, b, c, d, i))
	}
	return rx, ry
}
