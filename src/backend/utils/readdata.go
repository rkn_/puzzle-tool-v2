package utils

import (
	"encoding/csv"
	"strconv"
)

// ReadData read csv file data and return x,y array value
func ReadData(reader *csv.Reader) ([]string, []string) {
	// レコード読み取り
	var x []string
	var y []string
	for {
		// 行毎のデータ取得してlineに格納
		line, err := reader.Read()
		if err != nil {
			break
		}
		_, xerr := strconv.Atoi(line[0])
		_, yerr := strconv.Atoi(line[1])
		if xerr == nil && yerr == nil {
			x = append(x, line[0])
			y = append(y, line[1])
		}
	}
	return x, y
}
