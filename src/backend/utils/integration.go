package utils

import (
	"strconv"
)

// Integrate calclate splined function's area
func Integrate(x, y []float64, base []string) ([]float64, []float64) {
	var res, ratios []float64
	section := 0
	sum := 0.0
	sec := strconvint(base)
	sections := devideSections(x, sec)
	dx := 0.01
	all := x[len(x)-1] * 4.0
	for i := 0; i < len(x); i++ {
		if x[i] >= sections[section] {
			if sum < 0 {
				sum = 0
			}
			res = append(res, sum)
			ratios = append(ratios, sum)
			sum = 0.0
			if len(sections)-1 > section {
				section++
			}
		}
		sum += (4 - y[i]) * dx
	}
	for i := 0; i < len(res); i++ {
		ratios[i] = ratios[i] / all
	}
	return res, ratios
}

func devideSections(x []float64, sections []int) []float64 {
	xMax := x[len(x)-1]
	var res []float64
	for i := 0; i < len(sections); i++ {
		val := float64(sections[i])
		res = append(res, xMax*val/100.0)
	}
	res = append(res, xMax)
	return res
}

func strconvint(base []string) []int {
	var res []int
	for _, i := range base {
		val, _ := strconv.Atoi(i)
		res = append(res, val)
	}
	return res
}
