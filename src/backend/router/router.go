package router

import (
	"fmt"
	"os"
	"puzzle-tool-v2/src/backend/api"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// CreateRoute create routering
func CreateRoute() *gin.Engine {
	prjpath := os.Getenv("GOPATH") + "/src/puzzle-tool-v2/"

	fmt.Println(prjpath)
	router := gin.Default()

	// define session memory
	store := sessions.NewCookieStore([]byte("secret"))
	router.Use(sessions.Sessions("session", store))

	router.LoadHTMLFiles(prjpath + "dist/index.html")
	router.Static("static/css", prjpath+"dist/css")
	router.Static("static/js", prjpath+"dist/js")
	router.Static("static/fonts", prjpath+"dist/fonts")
	// デプロイ時
	// router.Static("/puzzle-tool/static/fonts", prjpath+"dist/static/fonts")
	router.Static("static/favicon.ico", prjpath+"dist/favicon.ico")

	router.GET("/", view)
	group := router.Group("api")
	{
		group.POST("/getfile", api.GetFile)
		group.POST("/login", api.Login)
		group.POST("/signup", api.Signup)
		group.POST("/auth", api.ConfirmSession)
		group.POST("/logout", api.Logout)
		group.POST("/startProcess", api.StartProcess)
		group.POST("/readPatients", api.ReadPatients)
		group.POST("/insertTimeStump", api.InsertTimeStump)
		group.POST("/readPassedPatient", api.ReadPassedPatient)
		group.POST("/readPatientData", api.ReadPatientData)
		group.POST("/deleteData", api.DeletePatientData)
	}
	return router
}

func view(ctx *gin.Context) {
	ctx.HTML(200, "index.html", gin.H{})
}
