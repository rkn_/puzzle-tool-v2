import Vue from 'vue';
import VueRouter from 'vue-router';
import Header from '@/frontend/components/header.vue'
import signup from '@/frontend/views/Signup.vue'
import Home from '@/frontend/views/logged/Home.vue'
import integral from '@/frontend/views/logged/integral.vue'
import logger from '@/frontend/views/logged/logger.vue'

Vue.use(VueRouter);

const routes = [
    {
        path: '/about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/About.vue'),
    },
    {
        path: '/header',
        name: 'header',
        component: Header,
        meta: {
            isPublic: false
        }
    },
    {
        path: '/signup',
        name: 'signup',
        component: signup,
        meta: {
            isPublic: true
        }    
    },
    {
        path: '/logged/home',
        name: 'Home',
        component: Home,
        meta: {
            isPublic: false
        }
    },
    {
        path: '/logged/integral',
        name: 'integral',
        component: integral,
        meta: {
            isPublic: false
        }
    },
    {
        path: '/logged/logger',
        name: 'logger',
        component: logger,
        meta: {
            isPublic: false
        }
    }
];

const router = new VueRouter({
    base: process.env.BASE_URL,
    routes,
});

export default router;