import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './quasar';
import { post } from '@/frontend/libs/post'
import VueApexCharts from 'vue-apexcharts'
import VueScrollTo from 'vue-scrollto'

Vue.use(VueScrollTo)
Vue.use(VueApexCharts)

Vue.component('apexchart', VueApexCharts)

Vue.config.productionTip = false;
router.beforeEach((to, _, next) => {
    let param = new URLSearchParams()
    param.append('sess', store.state.auth.token)
    post('/api/auth', param).then(res => {
        if (to.matched.some(record => !record.meta.isPublic) && !res.data) {
            next('/signup')
        } else {
            next()
        }
    })
})
new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');