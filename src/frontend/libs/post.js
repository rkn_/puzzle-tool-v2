import axios from 'axios'

export function post(link, param) {
    // deploy
    //  return axios.post('/puzzle-tool-v2/' + link, param)
    // dev
    return axios.post(link, param)
}