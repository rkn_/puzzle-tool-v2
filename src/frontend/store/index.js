import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        auth: {
            token: '',
            login: false
        },
        integral: {
            timeseries: [],
            date: []
        },
        home: {
            PatientID: ''
        },
        timestump: {
            containerID: ''
        },
        check: {
            val: [],
            date: []
        },
        stepper: {
            step: 1
        }
    },
    mutations: {
        login(state, token) {
            state.auth.token = token
            state.auth.login = true
        },
        setresult(state, res) {
            let last = res[res.length - 1][0]
            for (let i = 0; i < res.length; i++) {
                res[i][0] = (res[i][0] / last) * 100
            }
            state.integral.timeseries = res
        },
        setresultmulti(state, res) {
            state.integral.timeseries = res.eval
            state.integral.date = res.name
        },
        setPatientID(state, ID) {
            state.home.PatientID = ID
        },
        setAuth(state, Sess) {
            state.auth.token = Sess
            state.auth.login = true
        },
        logout(state) {
            state.auth.token = ''
            state.auth.login = false
        },
        setContainerID(state, containerID) {
            state.timestump.containerID = containerID
        },
        initialize(state) {
            state.integral = {
                timeseries: [],
                date: []
            }
            state.check = {
                val: [],
                date: []
            }
        },
        setcheck(state, obj) {
            state.check.val.push(obj.check)
            state.check.date.push(obj.date)
        }
    },
    actions: {},
    modules: {},
    getters: {
        getState(state) {
            return state
        }
    },
    plugins: [createPersistedState({ storage: window.sessionStorage })]
})